import './style.css'
import javascriptLogo from './javascript.svg'
import { setupCounter } from './counter.js'

document.querySelector('#app').innerHTML = `
  <div>
    <a href="https://vitejs.dev" target="_blank">
      <img src="" class="logo" alt="Vite logo" />
    </a>
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">
      <img src="https://m.media-amazon.com/images/I/61B1FkzhYAS._AC_SY450_.jpg" class="logo vanilla" alt="JavaScript logo" />
    </a>
    <h1>Qwick Qwack JS Snake Game</h1>
    <div class="card">
      <button id="counter" type="button"></button>
      <button id="counter" type="button"></button>

      <button id="counter" type="button"></button>

      <button id="counter" type="button"></button>

    </div>
    <p class="read-the-docs">
      SEE the SNAKE in WEB Navigator or in you TERMINAL Computer
    </p>
    <h1>This Snake Game has being coding with a Functional JavaScript implementation</h1>
  </div>
`

setupCounter(document.querySelector('#counter'))
